# SPKS-dataset

**SPKS: Surgical Procedural Knowledge Sentences dataset**. 

You can request a copy of this dataset filling [this](https://docs.google.com/forms/d/e/1FAIpQLSek71OkMhVBgtqAbEf_eZlktQ-Eeeaa6NrDNVX-qLhN6UoGgQ/viewform?usp=sf_link) form.

Introduction
----


SPKS is a textual dataset of the surgical robotic field consisting of 1958 sentences (37022 words and 3999 unique words) manually annotated as procedural and non-procedural by an expert annotator. To the best of our knowledge, SPKS is the first dataset in the literature containing procedural annotated sentences from the surgical robotic sector. We believe it is an indispensable resource for anyone who wants to test their classification algorithms in order to detect procedural knowledge in surgial written texts. 

Robotic surgery procedures described in SPKS
----
SPKS describes 20 different surgical procedures:

Urology:
- Partial nephrectomy (44 sentences)
- Radical prostatectomy (160 sentences)
- Adrenalectomy (24 sentences)
- Laparoscopic radical cystectomy and anterior extenteration for male patients (123 sentences)
- Ileal conduit extracorporal urinary diversion (143 sentences)
- Studer orthopic ileal neobladder formation (66 sentences)
- Indiana pouch formation (71 sentences)
- Intracorporeal urinary diversion (143 sentences)

Gynecology:
- Hysterectomy with bilateral salpingo oophorectomy (115 sentences)
- Radical hysterectomy (82 sentences)
- Sacrocolpopexy (104 sentences)

Gastrointestinal procedures:
- Total gastrectomy (95 sentences)
- Radical distal subtotal gastrectomy and D2 lymphadenectomy for gastric cancer (117 sentences)
- Cholecystectomy (170 sentences)
- Colectomy (217 sentences)

Thoracic procedures:
- Treatment of the anterior mediastinum (72 sentences)
- Robotic pulmonary resection (90 sentences)
- Robotic-Assisted minimally invasive esophagectomy (141 sentences)

Text sources
----
The sentences are taken from different sources which I list below:

-  Fong, Y., Woo, Y., Hyung, W., Lau, C., Strong, V.: The SAGES Atlas of Robotic Surgery (2018).

>**Considered chapters and sections**: 
<ins>Chapter 11</ins> (Port placement and instruments; Key Operative steps); <ins>Chapter 12</ins> (Room Setup and Positioning; Port Placement; Steps of Operation); <ins>Chapter 13</ins> (Key Operative Steps); <ins>Chapter 14</ins> (Technique and Steps of Radical Cystoprostatectomy in the Male Patient; Techniques and Steps of Anterior Exenteration/Radical Cystectomy in the Female Patient; Ileal Conduit Extracorporeal Urinary Diversion; Studer Orthotopic Ileal Neobladder Formation; Indiana Pouch Formation; Intracorporeal Urinary Diversion); <ins>Chapter 15</ins> (Technique and Steps of Pelivic Lymph Node Dissection; Technique and Steps of Retroperitoneal Lymph Node Dissection; Technique and Steps for Pelvic and/or Retroperitoneal Lymph Node Dissection for Renal Cell Carcinoma and Upper Tract Urothelial Carcinoma); <ins>Chapter 16</ins> (Robotic Hysterectomy and Bilateral Salpingo-Oophorectomy Procedure; Special Circumstances Encountered During Robotic Surgery); <ins>Chapter 17</ins> (Procedure; Operative Steps); <ins>Chapter 18</ins> (Indications for Surgery; Robotic Surgical Approach Considerations; Approach Options for RASC Combined with Hysterectomy; RASC with Hysterectomy Steps; RASC without Hysterectomy Steps; Postoperative Care); <ins>Chapter 19</ins> (Patient Selection; Procedural Details; Procedural Setup; Omentectomy; Dissection of the Grater Curvature; Division of the Proximal Duodenum; Lymphadenectomy; Division of the Distal Esophagus; Specimen Retrieval; Roux-En-Y Reconstruction; Postoperative Care and Complications; Postoperative Outcomes); <ins>Chapter 20</ins> (Procedure Preparation; Surgical Procedure Overview; Potential Complications); <ins>Chapter 21</ins> (Preoperative Considerations; Robotic Multiport Cholecystectomy; Robotic Single-Site Cholecystectomy; Procedure-Specific Complications; Postoperetive Care); <ins>Chapter 22</ins> (Indications; Preoperative Preparation; Robotic Right Colectomy; Robotic Left Colectomy and Sigmoidectomy; Postoperative Course) <!-- (This may be the most platform independent comment)-->
<!--> ; Outcomes-->
-  Sarkaria,  I.S.,  Rizk,  N.P.:  Robotic-assisted  minimally  invasive  esophagectomy:  The  ivorlewis approach (2014). 

- Savitt, M.A., Gao, G., Furnary, A.P., Swanson, J., Gately, H.L., Handy, J.R.: Application of  robotic-assisted  techniques  to  the  surgical  evaluation  and  treatment  of  the  anteriormediastinum (2005).

-  Pardolesi, A., Bertolaccini, L., Brandolini, J., Gallina, F., Novellis, P., Veronesi, G., Solli, P.: Four arms robotic-assisted pulmonary resection-right lower/middle lobectomy: How todo it. (2018)




Contact and cite us
----
If you are using the dataset please cite the following paper:

Bombieri, M., Rospocher, M., Dall’Alba, D. et al. Automatic detection of procedural knowledge in robotic-assisted surgical texts. Int J CARS (2021). https://doi.org/10.1007/s11548-021-02370-9

Please do not hesitate to contact us for clarifications and suggestions:
marco.bombieri_01@univr.it






